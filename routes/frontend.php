<?php

use App\Middleware\AuthIsGuest;
use Pecee\SimpleRouter\SimpleRouter as Route;

Route::group(['namespace' => '\App\Controllers\Frontend', 'as' => 'frontend'], function () {
    Route::get('/', 'TodoController@index')->name('todo.index');
    Route::get('/todo/sort/{field}', 'TodoController@sort')->name('todo.sort');

    Route::get('/todo/show/{id}', 'TodoController@show')->name('todo.show');

    Route::get('/todo/create', 'TodoController@create')->name('todo.create');
    Route::post('/todo/store', 'TodoController@store')->name('todo.store');

    /**
     * @description Admin routes
     */
    Route::get('/todo/edit/{id}', 'TodoController@edit')->name('todo.edit')->addMiddleware(AuthIsGuest::class);
    Route::post('/todo/update/{id}', 'TodoController@update')->name('todo.update')->addMiddleware(AuthIsGuest::class);
    Route::post('/todo/delete/{id}', 'TodoController@delete')->name('todo.delete')->addMiddleware(AuthIsGuest::class);
});