<?php

use App\Middleware\AuthIsAuthenticated;
use Pecee\SimpleRouter\SimpleRouter as Route;

Route::group(['namespace' => '\App\Controllers\Auth', 'prefix' => 'auth', 'as' => 'auth'], function () {
    Route::get('/login', 'AuthController@login_form')->addMiddleware(AuthIsAuthenticated::class);
    Route::post('/login', 'AuthController@login')->addMiddleware(AuthIsAuthenticated::class)->name('login');

    Route::post('/logout', 'AuthController@logout')->name('logout');
});