<?php

use Pecee\SimpleRouter\SimpleRouter as Route;
use Illuminate\Database\Capsule\Manager as Capsule;

/**
 * @description DEV routes
 */
Route::group(['namespace' => '\App\Controllers\System', 'prefix' => '/dev', 'as' => 'system'], function () {
    Route::all('/simple-migrate/{force?}', function ($force = false) {
        $errors = 0;

        /**
         * Create "TODO" table
         */
        if ($force) {
            Capsule::schema()->dropIfExists('todo');
            Capsule::schema()->dropIfExists('users');
            Capsule::schema()->dropIfExists('users_confirmations');
            Capsule::schema()->dropIfExists('users_remembered');
            Capsule::schema()->dropIfExists('users_resets');
            Capsule::schema()->dropIfExists('users_throttling');
        }

        if (!Capsule::schema()->hasTable('todo')) {
            Capsule::schema()->create('todo', function ($table) {
                $table->increments('id');
                $table->string('email');
                $table->string('username');
                $table->text('body');
                $table->string('status', 25)->default('new');
                $table->boolean('is_modified_by_admin')->default(false);
                $table->timestamps();
            });
        } else {
            $errors++;

            echo '<br>Table "todo" already exists';
        }

        $db = Capsule::schema()->getConnection();

        if (!Capsule::schema()->hasTable('users')) {
            $db->statement("
                CREATE TABLE IF NOT EXISTS `users` (
                  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                  `email` varchar(249) COLLATE utf8mb4_unicode_ci NOT NULL,
                  `password` varchar(255) CHARACTER SET latin1 COLLATE latin1_general_cs NOT NULL,
                  `username` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                  `status` tinyint(2) unsigned NOT NULL DEFAULT '0',
                  `verified` tinyint(1) unsigned NOT NULL DEFAULT '0',
                  `resettable` tinyint(1) unsigned NOT NULL DEFAULT '1',
                  `roles_mask` int(10) unsigned NOT NULL DEFAULT '0',
                  `registered` int(10) unsigned NOT NULL,
                  `last_login` int(10) unsigned DEFAULT NULL,
                  `force_logout` mediumint(7) unsigned NOT NULL DEFAULT '0',
                  PRIMARY KEY (`id`),
                  UNIQUE KEY `email` (`email`)
                ) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
            ");
        } else {
            $errors++;

            echo '<br>Table "users" already exists';
        }

        if (!Capsule::schema()->hasTable('users_confirmations')) {
            $db->statement("
                CREATE TABLE IF NOT EXISTS `users_confirmations` (
                  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                  `user_id` int(10) unsigned NOT NULL,
                  `email` varchar(249) COLLATE utf8mb4_unicode_ci NOT NULL,
                  `selector` varchar(16) CHARACTER SET latin1 COLLATE latin1_general_cs NOT NULL,
                  `token` varchar(255) CHARACTER SET latin1 COLLATE latin1_general_cs NOT NULL,
                  `expires` int(10) unsigned NOT NULL,
                  PRIMARY KEY (`id`),
                  UNIQUE KEY `selector` (`selector`),
                  KEY `email_expires` (`email`,`expires`),
                  KEY `user_id` (`user_id`)
                ) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
            ");
        } else {
            $errors++;

            echo '<br>Table "users_confirmations" already exists';
        }

        if (!Capsule::schema()->hasTable('users_remembered')) {
            $db->statement("
                CREATE TABLE IF NOT EXISTS `users_remembered` (
                  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
                  `user` int(10) unsigned NOT NULL,
                  `selector` varchar(24) CHARACTER SET latin1 COLLATE latin1_general_cs NOT NULL,
                  `token` varchar(255) CHARACTER SET latin1 COLLATE latin1_general_cs NOT NULL,
                  `expires` int(10) unsigned NOT NULL,
                  PRIMARY KEY (`id`),
                  UNIQUE KEY `selector` (`selector`),
                  KEY `user` (`user`)
                ) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
            ");
        } else {
            $errors++;

            echo '<br>Table "users_remembered" already exists';
        }

        if (!Capsule::schema()->hasTable('users_resets')) {
            $db->statement("
                CREATE TABLE IF NOT EXISTS `users_resets` (
                  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
                  `user` int(10) unsigned NOT NULL,
                  `selector` varchar(20) CHARACTER SET latin1 COLLATE latin1_general_cs NOT NULL,
                  `token` varchar(255) CHARACTER SET latin1 COLLATE latin1_general_cs NOT NULL,
                  `expires` int(10) unsigned NOT NULL,
                  PRIMARY KEY (`id`),
                  UNIQUE KEY `selector` (`selector`),
                  KEY `user_expires` (`user`,`expires`)
                ) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
            ");
        } else {
            $errors++;

            echo '<br>Table "users_resets" already exists';
        }
        if (!Capsule::schema()->hasTable('users_throttling')) {
            $db->statement("
                CREATE TABLE IF NOT EXISTS `users_throttling` (
                  `bucket` varchar(44) CHARACTER SET latin1 COLLATE latin1_general_cs NOT NULL,
                  `tokens` float unsigned NOT NULL,
                  `replenished_at` int(10) unsigned NOT NULL,
                  `expires_at` int(10) unsigned NOT NULL,
                  PRIMARY KEY (`bucket`),
                  KEY `expires_at` (`expires_at`)
                ) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
            ");
        } else {
            $errors++;

            echo '<br>Table "users_throttling" already exists';
        }

        return !$errors ? '<br>OK' : '<br>FAILS';
    });

    Route::all('/create-user', function () {
        try {
            app()->auth()->register('admin@site.com', '123', 'admin');

            echo 'We have signed up a new user with "username" => "admin"';

            app()->auth()->login('admin@site.com', '123');
        } catch (\Delight\Auth\UserAlreadyExistsException $e) {
            app()->auth()->login('admin@site.com', '123');

            die('User already exists');
        }

        return 'OK';
    });
});