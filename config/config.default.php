<?php

return [
    'debug' => false,
    'site' => [
        'title' => 'App Name',
        'items_per_page' => 3,
    ],
    'vendor_path' => __DIR__ . '/../vendor',
    'twig' => [
        'path_to_cache' => __DIR__ . '/../cache/twig',
        'path_to_templates' => __DIR__ . '/../templates',

    ],
    'db' => [
        'driver' => 'mysql',
        'host' => 'localhost',
        'database' => 'db_name',
        'username' => 'db_username',
        'password' => 'db_password',
        'charset' => 'utf8mb4',
        'collation' => 'utf8mb4_unicode_ci',
    ],
];