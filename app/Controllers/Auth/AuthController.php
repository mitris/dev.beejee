<?php

namespace App\Controllers\Auth;

use App\Controllers\BaseController;
use App\Validation\Auth as AuthValidator;
use Rakit\Validation\Validator;


class AuthController extends BaseController
{

    public function login_form()
    {
        return $this->render('auth/login');
    }

    public function login()
    {
        $validator = new Validator;
        $validator->addValidator('auth', new AuthValidator());

        $validation = $validator->make(input()->all(), [
            'username' => 'required|auth',
            'password' => 'required',
        ], [
            'required' => ':attribute поле обзязательно для заполнения',
            'email' => ':email нее верный формат',
        ]);

        $validation->validate();

        if ($validation->fails()) {
            return $this->render('auth/login', ['errors' => $validation->errors()]);
        } else {
            return redirect(url('/'));
        }
    }

    public function logout()
    {
        app()->auth()->logOut();

        return redirect('/');
    }

}