<?php

namespace App\Controllers;

class BaseController
{

    final protected function render($template_name, $parameters = [])
    {
        $template = app()->view()->load($template_name . '.twig');

        return $template->render($parameters);
    }

}