<?php

namespace App\Controllers\Frontend;

use App\Controllers\BaseController;
use App\Models\Todo;
use Rakit\Validation\Validator;

class TodoController extends BaseController
{
    public function index(): string
    {
        $filter = [
            'order_by' => 'username',
            'order' => 'asc',
            'per_page' => env('SITE_ITEMS_PER_PAGE'),
        ];

        $filter = array_merge($filter, (array)get('filter'));

        $validator = new Validator();
        $validation = $validator->make($filter, [
            'order_by' => 'in:email,username,status',
            'order' => 'in:asc,desc',
            'per_page' => 'numeric',
        ]);
        $validation->validate();

        $query = Todo::query();

        if ($validation->passes()) {
            $query->orderBy($filter['order_by'], $filter['order']);
            $paginator = $query->paginate((string)$filter['per_page']);
        } else {
            $paginator = $query->paginate(env('SITE_ITEMS_PER_PAGE'));
        }

        $paginator->appends(['filter' => $filter]);

        return $this->render('frontend/todo/index', [
            'paginator' => $paginator,
            'errors' => $validation->errors(),
            'filter' => $filter,
            'tab_action' => 'index'
        ]);
    }

    public function sort($field)
    {
        $filter = [
            'order_by' => $field,
            'order' => $field == get('filter.order_by') ? get('filter.order') == 'asc' ? 'desc' : 'asc' : 'asc',
            'per_page' => get('filter.per_page'),
        ];

        return redirect(url('/', [], ['filter' => $filter]));
    }

    public function create()
    {
        return $this->render('frontend/todo/create', ['tab_action' => 'create']);
    }

    public function store()
    {
        $validation_errors = $this->validate();

        if ($validation_errors) {
            return $this->render('frontend/todo/create', ['errors' => $validation_errors, 'tab_action' => 'create']);
        } else {
            $todo = new Todo();
            $todo->email = input('email');
            $todo->username = input('username');
            $todo->body = input('body');
            $todo->save();

            flash('Задача успешно создана', 'success');

            return redirect(url('/'));
        }
    }

    public function show($id)
    {
        $todo = Todo::findOrFail($id);

        return $this->render('frontend/todo/show', ['todo' => $todo, 'tab_action' => 'show']);
    }

    public function edit($id)
    {
        $todo = Todo::findOrFail($id);

        return $this->render('frontend/todo/edit', ['todo' => $todo, 'tab_action' => 'edit', 'statuses' => \App\Models\Todo::getStatuses()]);
    }

    public function update($id)
    {
        $validation_errors = $this->validate();

        $todo = Todo::findOrFail($id);

        if ($validation_errors) {
            return $this->render('frontend/todo/edit', ['errors' => $validation_errors, 'tab_action' => 'edit', 'todo' => $todo, 'id' => $id]);
        } else {
            $todo->email = input('email');
            $todo->username = input('username');
            if (!$todo->is_modified_by_admin) {
                $todo->is_modified_by_admin = $todo->body != input('body');
            }
            $todo->body = input('body');
            $todo->status = input('status');
            $todo->save();

            flash('Задача успешно обновлена', 'success');

            return redirect(url('/'));
        }
    }

    public function delete($id)
    {
        $todo = Todo::findOrFail($id);
        $todo->delete();

        flash('Задача успешно удалена', 'success');

        return redirect(url('/'));
    }

    protected function validate()
    {
        $validator = new Validator;

        $validation = $validator->make(input()->all(), [
            'email' => 'required|email',
            'username' => 'required',
            'body' => 'required',
        ], [
            'required' => ':attribute обзязательно для заполнения',
            'email' => ':attribute не верный формат',
        ]);

        $validation->validate();

        return $validation->fails() ? $validation->errors() : false;
    }
}