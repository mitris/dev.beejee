<?php

namespace App\Validation;

use Delight\Auth\UnknownUsernameException;
use Delight\Auth\InvalidPasswordException;
use Rakit\Validation\Rule;

class Auth extends Rule
{
    protected $message = "Введен неверный логин или пароль";

    public function check($value): bool
    {
        try {
            $auth = app()->auth();

            $rememberDuration = (int)(60 * 60 * 24 * 365.25);

            $auth->loginWithUsername(input('username'), input('password'), input('remember') ? $rememberDuration : false);

            return true;
        } catch (UnknownUsernameException $e) {
            return false;
        } catch (InvalidPasswordException $e) {
            return false;
        }
    }
}