<?php

namespace App\Middleware;

use Pecee\Http\Middleware\IMiddleware;
use Pecee\Http\Request;

class AuthIsAuthenticated implements IMiddleware
{
    public function handle(Request $request): void
    {
        if (app()->auth()->check() === true) {
            $request->setRewriteUrl(url('/'));
        }
    }

}