<?php

namespace App\Middleware;

use Pecee\Http\Middleware\IMiddleware;
use Pecee\Http\Request;

class AuthIsGuest implements IMiddleware
{
    public function handle(Request $request): void
    {
        if (app()->auth()->check() === false) {
            $request->setRewriteUrl(url('auth.login'));
        }
    }

}