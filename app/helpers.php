<?php

function app()
{
    return App\App::$context;
}

function auth()
{
    return app()->auth();
}

function get($index, $default = null)
{
    $input = input()->all();
    $part = strtok($index, '.');

    while ($part !== false) {
        if (!isset($input[$part])) {
            return $default;
        }
        $input = $input[$part];
        $part = strtok('.');
    }

    return $input;
}

function pagination($paginator)
{
    $html[] = '';
    if ($paginator->hasPages()) {
        $html[] = '<ul class="pagination" role="navigation">';
        if ($paginator->onFirstPage()) {
            $html[] = '<li class="page-item disabled" aria-disabled="true">';
            $html[] = '<span class="page-link" aria-hidden="true">&lsaquo;</span>';
            $html[] = '</li>';
        } else {
            $html[] = '<li class="page-item">';
            $html[] = '<a class="page-link" href="' . $paginator->previousPageUrl() . '" rel="prev">&lsaquo;</a>';
            $html[] = '</li>';
        }

        foreach ($paginator->getUrlRange(1, $paginator->lastPage()) as $page => $url) {
            if ($page == $paginator->currentPage()) {
                $html[] = '<li class="page-item active" aria-current="page"><span class="page-link">' . $page . '</span></li>';
            } else {
                $html[] = '<li class="page-item"><a class="page-link" href="' . $url . '">' . $page . '</a></li>';
            }
        }

        if ($paginator->hasMorePages()) {
            $html[] = '<li class="page-item">';
            $html[] = '<a class="page-link" href="' . $paginator->nextPageUrl() . '" rel="next" >&rsaquo;</a>';
            $html[] = '</li>';
        } else {
            $html[] = '<li class="page-item disabled" aria-disabled="true">';
            $html[] = '<span class="page-link" aria-hidden="true">&rsaquo;</span>';
            $html[] = '</li>';
        }
        $html[] = '</ul>';
    }

    return implode($html, '');
}

