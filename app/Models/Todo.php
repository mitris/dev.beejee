<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Todo extends Model
{

    public static $statuses = [
        'new' => 'Новая',
        'completed' => 'Выполнено',
    ];

    protected $table = 'todo';

    public function getStatus()
    {
        return isset(self::$statuses[$this->status]) ? self::$statuses[$this->status] : 'N/A';
    }

    public static function getStatuses()
    {
        return self::$statuses;
    }

}
