<?php

namespace App;

use App\Middleware\CsrfTokenVerify;
use Dotenv\Dotenv;
use Delight\Auth\Auth;
use Illuminate\Pagination\Paginator;
use Whoops\Run as WhoopsRun;
use Whoops\Handler\PrettyPageHandler as WhoopsPrettyPageHandler;
use Pecee\SimpleRouter\SimpleRouter as Route;
use Twig\TwigFunction;
use Twig\Environment as TwigEnvironment;
use Twig\Extension\DebugExtension as TwigDebugExtension;
use Twig\Loader\FilesystemLoader as TwigFilesystemLoader;
use Illuminate\Database\Capsule\Manager as DB;

class App
{

    protected $db;

    protected $view;

    protected $auth;

    public static $context;

    /**
     * @desrription Register main functionality
     */

    public function __construct()
    {
        self::$context = $this;

        $this->registerErrorHandler();

        $this->setEnv();

        require_once __DIR__ . '/helpers.php';

        $this->initDatabaseConnection();

        $this->initViewEngine();

        $this->initAuth();

        $this->registerRoutes();
    }

    /**
     * @description Set env variables
     */

    protected function registerErrorHandler()
    {
        $whoops = new WhoopsRun;
        $whoops->pushHandler(new WhoopsPrettyPageHandler);
        $whoops->register();
    }

    /**
     * @description Register routes
     */

    protected function registerRoutes()
    {
        Route::csrfVerifier(new CsrfTokenVerify);

        require_once '../routes/auth.php';
        require_once '../routes/frontend.php';
        require_once '../routes/system.php';

        Route::start();
    }

    /**
     * @description Set env variables
     */

    protected function setEnv()
    {
        $env = self::array_recursive(require_once '../config/config.php');

        foreach ($env as $key => $value) {
            putenv("$key=$value");
        }

        $env = Dotenv::create(__DIR__ . '/../');
        $env->overload();
    }

    /**
     * @description Register DB connection
     */

    protected function initDatabaseConnection()
    {
        $capsule = new DB;

        $capsule->addConnection([
            'driver' => env('DB_DRIVER'),
            'host' => env('DB_HOST'),
            'database' => env('DB_DATABASE'),
            'username' => env('DB_USERNAME'),
            'password' => env('DB_PASSWORD'),
            'charset' => env('DB_CHARSET'),
            'collation' => env('DB_COLLATION'),
        ]);

        $capsule->setAsGlobal();
        $capsule->bootEloquent();

        Paginator::currentPageResolver(function () {
            return input('page');
        });

        $this->db = $capsule;
    }

    protected function initAuth()
    {
        $this->auth = new Auth($this->db->getConnection()->getPdo());
    }

    /**
     * @description Init view engine
     */

    protected function initViewEngine()
    {
        $loader = new TwigFilesystemLoader(env('TWIG_PATH_TO_TEMPLATES'));

        $this->view = new TwigEnvironment($loader, [
            'cache' => env('DEBUG') ? false : env('TWIG_PATH_TO_CACHE'),
            'debug' => env('DEBUG'),
            'auto_reload' => env('DEBUG'),
//            'autoescape' => false,
        ]);

        $this->view->addFunction(new TwigFunction('env', function ($key, $default = null) {
            return env($key, $default);
        }));

        $this->view->addFunction(new TwigFunction('flash', function ($message = null, $type = null) {
            return flash($message, $type);
        }));

        $this->view->addFunction(new TwigFunction('app', function () {
            return app();
        }));

        $this->view->addFunction(new TwigFunction('auth', function () {
            return auth();
        }));

        $this->view->addFunction(new TwigFunction('csrf_token', function () {
            return csrf_token();
        }));

        $this->view->addFunction(new TwigFunction('url', function ($name = null, $parameters = null, $getParams = null) {
            return url($name, $parameters, $getParams);
        }));

        $this->view->addFunction(new TwigFunction('pagination', function ($pagination) {
            return pagination($pagination);
        }));

        $this->view->addFunction(new TwigFunction('get', function ($index = null, $default = null) {
            return get($index, $default);
        }));

        $this->view->addExtension(new TwigDebugExtension());
    }

    /**
     * @param $array
     * @param null $pre_key
     * @return array
     * @description Helper function
     */

    public static function array_recursive($array, $pre_key = null)
    {
        $flat = [];

        $pre_key = is_null($pre_key) ? '' : $pre_key . '_';

        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $flat = array_merge($flat, self::array_recursive($value, $pre_key . $key));
            } else {
                if (is_bool($value)) {
                    switch ($value) {
                        case true:
                            $value = 'true';
                            break;
                        case false:
                            $value = 'false';
                            break;
                    }
                }

                $flat[mb_strtoupper($pre_key . $key)] = $value;
            }
        }
        return $flat;
    }

    /**
     * @description Return DB instance
     */
    public function db()
    {
        return $this->db;
    }

    /**
     * @description Return View instance
     */
    public function view()
    {
        return $this->view;
    }

    /**
     * @description Return Auth/User instance
     */
    public function auth()
    {
        return $this->auth;
    }

}